from django.forms import ModelForm
from receipts.models import Account, Receipt, ExpenseCategory
# Forms show on your ADMIN page.


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",     # These two fields come from the class Account in models.py.
            "number",
        ]


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name"      # These two fields come from the class Account in models.py.
        ]
